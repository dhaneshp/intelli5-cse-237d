Course: CSE 237D
Project : Man vs Bot
Team: Intelli5
Dhanesh Pradhan
Yashank Sakhardande
Mihir Patankar
Vignesh Srinivasan

Project Overview :

We developed an autonomous robotic companion which can be used to play a pencil-paper

game such as tic-tac-toe, chess, scrabble with a human. 

Sounds exciting ?! Check out our Wiki for a detailed overview.